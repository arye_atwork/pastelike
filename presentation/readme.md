
Generate HTML presentation with
 
    $landslide docker_introduction.cfg 

Add theme with:

    $landslide docker_introduction.md --relative --copy-theme

Embed images:

    $landslide docker_introduction.md --relative --copy-theme -i
