import pytest
from selenium.webdriver import Firefox


@pytest.fixture(scope='session')
def webdriver(request):
    driver = Firefox()
    request.addfinalizer(driver.quit)
    return driver


@pytest.mark.parametrize("test_input,expected", [
    ('https://nixos.org/nix/', 'Nix'),
    ('https://www.bing.com', 'Bing'),
])
def test_nix_website_title(test_input, expected, webdriver):
    webdriver.get(test_input)
    assert expected in webdriver.title


def test_pytest_website_title(webdriver):
    webdriver.get('http://pytest.org/latest/')
