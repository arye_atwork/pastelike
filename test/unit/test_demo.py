import unittest


def upper_reverse(text):
    return ''.join(reversed(text.upper()))


# before pytest
class TestUpperReverse(unittest.TestCase):
    def test_upper_reverse(self):
        self.assertEqual(upper_reverse('hello'), 'OLLEH')


# with pytest
def test_upper_reverse():
    assert upper_reverse('hello') == 'OLLEH'